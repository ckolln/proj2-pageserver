from flask import Flask, abort, render_template, request, make_response
import os
app = Flask(__name__)

@app.route("/<path:p>", methods=['GET'])
def index(p):
    fname = os.path.basename(p)
    if (("//" in p) or ("~" in p) or (".." in p)):
        abort(403)
        return None
    elif (not os.path.isfile("./templates/" + fname)):
        abort(404)
        return None
    else:
        response = make_response(render_template(fname),200)
        response.headers['X-Okay'] = '200/OK'
        return response

    # abort(403)

@app.errorhandler(404)
def error_404(e):
	return render_template('404.html'), 404

@app.errorhandler(403)
def error_403(e):
    return render_template('403.html'), 403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
